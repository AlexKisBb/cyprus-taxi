const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');
const mongoose = require('mongoose');
const bCrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const admin = require('firebase-admin');
var serviceAccount = require("C:/Users/Administrator/git-cyprus-taxi/borya-24cb3-firebase-adminsdk-ayqe4-c1c6e6be73.json");
var bsg = require( 'bsg-nodejs' )( 'live_yWqAqLzqj7iLyqpYXSL6' );

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://borya-24cb3.firebaseio.com"
});
const jwtSecret  = "secrethuikred";
const saltRounds = 10;

const multer = require('multer');
const path = require('path');
const fs = require('fs');
//схема для скачки картинок ----------------------------------------------------
const Schema = mongoose.Schema;
const schema = new Schema(
  {
    post: {
      type: Schema.Types.ObjectId,
      ref: 'Post'
    },
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    path: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);
schema.set('toJSON', {
  virtuals: true
});
//------------------------------------------------------------------------------

const storage = multer.diskStorage({
  destination: (req, file, cb) =>{
    cb(null, 'uploads');

  },
  filename: (req, file, cb) =>{
    const pathName = Date.now() + path.extname(file.originalname);
    cb(null,Date.now() + path.extname(file.originalname));
  }
});
const upload = multer({
  storage,
  limits:{  fileSize: 10 * 1024 * 1024  },
  fileFilter: (req, file, cb) => {
    const ext = path.extname(file.originalname);
    if(ext !== '.jpg' && ext !== '.png' && ext !== '.jpeg' && ext !== '.JPG' && ext !== '.PNG'){
      const err = new Error('Extention');
      err.code = 'EXTENTION';
      return cb(err);
    }
    cb(null, true)
  }
}).single('file');


mongoose.connect('mongodb://localhost:27017/cyprus-taxi-db');
//balance request model --------------------------------------------------------
const BalanceRequests = mongoose.model('BalanceRequests',{
  userId: String,
  userName: String,
  moneyValue: String,
  statusBalance: String,
  dateBalance: String,
  accepterOrderBalance: String,
  ownerOrderBalance: String,
  postIdTransaction: String,
});

//withrawal request model ------------------------------------------------------
const WithdrawalRequest = mongoose.model('WithdrawalRequest',{
  userId: String,
  userEmail: String,
  userName: String,
  userBalanceWithrawal: String,
  creditCard: String,
  requestDate: String,
  withdrawalStatus: String,
});

//admin model ------------------------------------------------------------------
const Admin = mongoose.model('Admin',{
  login: String,
  pass: String,
});
//users model ------------------------------------------------------------------
const Users = mongoose.model('Users',{
  email: String,
  pass: String,
  namesurname: String,
  phone: String,
  carmark: String,
  carmodel: String,
  userAvatar: String,
  userStatus: String,
  userBalance: String,
  fToken: String,
  userBalanceBlock: String,
});

const Orders = mongoose.model('Orders',{
  pointA: String,
  pointB: String,
  date: String,
  amountPeople: String,
  price: String,
  priceOrder: String,
  comment: String,
  babyChair: String,
  babySmallChair: String,
  ownerOrder: String,
  ownerAvatar: String,
  ownerId: String,
  accepter: String,
  accepterId: String,
  firslLt: String,
  firslLg: String,
  secondLt: String,
  secondLg: String,
  status: String,
  phoneClient: String,
  unixMcOrder: String,
  blMoneyFromTheCreator: String,
});

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(express.static(__dirname + '/uploads'));

//sending sms-------------------------------------------------------------------

function sendSms(smsText){
  const timeInMss = new Date().getTime()
  bsg.createSMS(
    {
      destination: "phone",
      originator:"testsms", //CyprusTaxi  testsms
      body:smsText,
      msisdn:"000000000",  //change number
      reference: timeInMss,
      validity:"1",
      tariff:"9"
    }
  ).then(
    SMS => console.log( "SMS created:", SMS ),
    error => console.log( "SMS creation failed:", error )
  );
}

//sending push notification ----------------------------------------------------
function sendPush(tk, titleMsg){
  var registrationToken = 'YOUR_REGISTRATION_TOKEN';
  var message = {
    data: {
      title: titleMsg
    },
    token: tk
  };
  // Send a message to the device corresponding to the provided
  // registration token.
  admin.messaging().send(message)
  .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', response);
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  });
}
//put firebaseToken to user profile --------------------------------------------
app.put('/testArrayOfObjects/:_id/addFirebaseToken', (req, res) =>{
  Users.findByIdAndUpdate({_id:req.params._id}, req.body)
  .then(function(){
    Users.findOne({_id:req.params._id}).then(
      createdRecord => res.json(createdRecord)
    )
  })
}
);
//midleware --------------------------------------------------------------------
const auteMidleware = (req, res, next) => {
  const authHeader = req.get('Authorization');
  if(!authHeader){
    res.status(401).json({massege: "Token is not valid"});
  }
  const token = authHeader.replace('Breare ', '');
  try{
    jwt.verify(token, jwtSecret);
  }catch (e){
    if(e instanceof jwt.JsonWebTokenError){
      res.status(401).json({message: 'Invalid token'})
    }
  }
  next();
};
//авторизация ------------------------------------------------------------------
const signIn = (req, res) => {
  const {email, pass} = req.body;
  Users.findOne({email})
  .exec()
  .then(user => {
    if (!user){
      res.status(401).json({massege: "User does not exist"});
    }
    // else if (user.userStatus == 'userOnModeration'){
    //   res.status(401).json({massege: "Учетная запись на модерации"});
    // }
    const isValid = bCrypt.compareSync(pass, user.pass);
    if (isValid){
      const token = jwt.sign({user}, jwtSecret);
      res.json([{token},user]);
    }
    else{
      res.status(401).json({massege: "Invalid credentials"});
    }
  })
  .catch(err => res.status(500).json({message: err.message}))
}

app.post('/signIn', signIn);

// user registration ----------------------------------------------------------

app.get('/testArrayOfObjects', (req, res) =>  Users.find()
.exec()
.then(testArrayOfObjects => res.json(testArrayOfObjects)),
);

//get one user ----------------------------------------------------------------
app.get('/testArrayOfObjects/:_id', (req, res) =>{
  Users.findOne({_id:req.params._id}).then(
    createdRecord => res.json(createdRecord)
  );
}
);

app.post('/testArrayOfObjects', (req, res) =>

Users.findOne({email: req.body.email}, function(err, user){
  if(err) {
    console.log(err);
  }
  if(user) {
    res.json({message: "user exists"});

  } else {
    //устанавливаем баланс
    req.body.userBalance = "0";
    req.body.userBalanceBlock = "0";
    //хешируем пароль
    bCrypt.hash(req.body.pass, saltRounds, function(err, hash) {
      req.body.pass = hash;
      // Store hash in your password DB.
      Users.create(req.body, function(err, createdUser){
        res.json(createdUser);
        sendSms("Новый запрос на регистрацию");
      })

    })
  }
})
);
//change user userStatus
app.put('/testArrayOfObjects/:_id/acceptanceUser', (req, res) =>{
  Users.findByIdAndUpdate({_id:req.params._id}, req.body)
  .then(function(){
    Users.findOne({_id:req.params._id}, function(err, user){
      if(user.fToken !== undefined && user.fToken !== null && user.fToken !== ''){
        sendPush(user.fToken,'regApproveNotify');}
        //  (user) => {res.json(user)};
      }).then(user => res.json(user))
    })
  }
);

//change user userStatus
app.put('/testArrayOfObjects/:_id/toBlackListUser', (req, res) =>{
  Users.findByIdAndUpdate({_id:req.params._id}, req.body)
  .then(function(){
    Users.findOne({_id:req.params._id}, function(err, user){
      if(user.fToken !== undefined && user.fToken !== null && user.fToken !== ''){
        sendPush(user.fToken,'regApproveNotify');}
        //  (user) => {res.json(user)};
      }).then(user => res.json(user))
    })
  }
);

//approve user balance
app.put('/testArrayOfObjects/:_id/setBalance', (req, res) =>{

  Users.findByIdAndUpdate({_id:req.params._id}, req.body)
  .then(function(){
    Users.findOne({_id:req.params._id}, function(err, user){
      if(user.fToken !== undefined && user.fToken !== null && user.fToken !== ''){
        sendPush(user.fToken,'addBalanceNotify');
      }
    }).then(createdUser => res.json(createdUser))
  })
}
);

//change user balance
app.post('/changeUsersBalance', (req, res) =>

Users.findOne({_id:req.body.accepterId}, function(err, userAccepter){
  Users.findOne({_id:req.body.ownerId}, function(err, userOwner){
    Users.findByIdAndUpdate({_id:req.body.accepterId}, {userBalance: +userAccepter.userBalance-(+req.body.valueCoastOrder)}).then(
      function(){
        Users.findByIdAndUpdate({_id:req.body.ownerId}, {userBalance: +userOwner.userBalance+(+req.body.valueCoastOrder)}).then(
          createdRecord => res.status(200).json({message: 'Успешное пополнение'})
        )}
      )
    })
  })
);

//create admin -----------------------------------------------------------------
app.post('/createAdmin', (req, res) =>

Admin.findOne({login: req.body.login}, function(err, user){

  if(err) {
    console.log(err);
  }

  if(user) {
    res.json({message: "user exists"});

  } else {
    //хешируем пароль
    bCrypt.hash(req.body.pass, saltRounds, function(err, hash) {
      req.body.pass = hash;
      // Store hash in your password DB.
      Admin.create(req.body)
      .then(createdUser => res.json(createdUser))
    })
  }
})
);
//авторизация admin-------------------------------------------------------------
const signInAdmin = (req, res) => {
  const {login, pass} = req.body;
  Admin.findOne({login})
  .exec()
  .then(user => {
    if (!user){
      res.status(401).json({massege: "User does not exist"});
    }
    const isValid = bCrypt.compareSync(pass, user.pass);
    if (isValid){
      const token = jwt.sign({user}, jwtSecret);
      res.json([{token}]);
    }
    else{
      res.status(401).json({massege: "Invalid credentials"});
    }
  })
  .catch(err => res.status(500).json({message: err.message}))
}

app.post('/signInAdmin', signInAdmin);
// orders ----------------------------------------------------------------------
app.get('/createOrder',(req,res) =>
Orders.find()
.exec()
.then(createdOrder => res.json(createdOrder)),
);
//get one order ----------------------------------------------------------------


app.get('/createOrder/:_id', (req, res) =>{
  Orders.findOne({_id:req.params._id}).then(
    createdRecord => res.json(createdRecord)
  );
}
);

function changeStatusThroughTime (timeMs,idParam){
  setTimeout(() => {
    Orders.findOne({_id:idParam}, function(err, orderUnix){
      if(orderUnix.status == "new"){
        Orders.findByIdAndUpdate({_id:idParam},{status:"expired"}).then(
          function(){
            Users.findOne({_id:orderUnix.ownerId}, function(err, userOwner){
              if(userOwner.fToken !== undefined && userOwner.fToken !== null && userOwner.fToken !== ''){
                sendPush(userOwner.fToken,'orderCanceledTimeNotify');}
              })
            }
          );
        }
      })
    },timeMs)

  }

  app.post('/createOrder', (req, res) =>
  Orders.create(req.body)
  .then(createdOrder => {res.json(createdOrder);
    changeStatusThroughTime (+createdOrder.unixMcOrder,createdOrder._id);
    sendSms("Новый заказ ожидает подтверждения");
  }
)
);

app.put('/createOrder/:_id/acceptance', (req, res) =>{
  Orders.findByIdAndUpdate({_id:req.params._id}, req.body)
  .then(function(){
    Orders.findOne({_id:req.params._id}, function(err, order){
      if(order.status == "inwork"){
        Users.findOne({_id:order.ownerId}, function(err, userOwner){
          if(userOwner.fToken !== undefined && userOwner.fToken !== null && userOwner.fToken !== ''){
            sendPush(userOwner.fToken,'orderAccNotify');}
          })
        }
      }).then(
        createdRecord => res.json(createdRecord)
      )
    })
  }
);

//отмена заказа ----------------------------------------------------------------
app.put('/createOrder/:_id/canceled', (req, res) =>{
  Orders.findByIdAndUpdate({_id:req.params._id}, req.body)
  .then(function(){
    Orders.findOne({_id:req.params._id}, function(err, order){
      Users.findOne({_id:order.ownerId}, function(err, userOwner){
        if(order.blMoneyFromTheCreator === 'true') {
          //проводим обнуление заблокированых средств у создателя-----------------
          Users.findByIdAndUpdate({_id:order.ownerId},
            {userBalanceBlock: +userOwner.userBalanceBlock - (+order.priceOrder),
              userBalance: +userOwner.userBalance + (+order.priceOrder)
            }).then(createdUser => {
              if(userOwner.fToken !== undefined && userOwner.fToken !== null && userOwner.fToken !== ''){
                if(order.status == 'cancellationAcceptor'){
                  sendPush(userOwner.fToken,'orderCanceledAccNotify');
                }
              }
              res.json(createdUser);});
            }
            else {createdUser => {
              if(userOwner.fToken !== undefined && userOwner.fToken !== null && userOwner.fToken !== ''){
                if(order.status == 'cancellationAcceptor'){
                  sendPush(userOwner.fToken,'orderCanceledAccNotify');
                }
              }
              res.json(createdUser);}
            }
          })
        }).then(createdUser => res.json(createdUser))
      })
    });
    //подтверждение выполнения заказа ----------------------------------------------
    app.put('/createOrder/:_id/finishedOrder', (req, res) =>{
      Orders.findByIdAndUpdate({_id:req.params._id}, req.body)
      .then(function(){
        Orders.findOne({_id:req.params._id}, function(err, order){
          switch (order.status) {
            case 'cancellationAcceptor':
            Users.findOne({_id:order.ownerId}, function(err, userOwner){
              if(userOwner.fToken !== undefined && userOwner.fToken !== null && userOwner.fToken !== ''){
                sendPush(userOwner.fToken,'orderCanceledAccNotify');}
              })
              break;
              case 'finished':
              //проверка если деньги не у клиента в момент завершения обнуляем блокировку средств
              if(order.blMoneyFromTheCreator === 'false'){
                Users.findOne({_id:order.ownerId}, function(err, userOwner){
                  if(userOwner.fToken !== undefined && userOwner.fToken !== null && userOwner.fToken !== ''){
                    sendPush(userOwner.fToken,'orderFinishedNotify');}
                  })
                }
                if(order.blMoneyFromTheCreator === 'true') {
                  //проводим обнуление заблокированых средств у создателя-------------------
                  Users.findOne({_id:order.ownerId}, function(err, user){

                    Users.findByIdAndUpdate({_id:order.ownerId},
                      {userBalanceBlock: +user.userBalanceBlock - (+order.priceOrder)
                        //далее проводим зачисление средств на счет исполнителя
                      }).then(
                        Users.findOne({_id:order.accepterId}, function(err, userAccept){
                          console.log(userAccept);
                          Users.findByIdAndUpdate({_id:order.accepterId},
                            {userBalance: +userAccept.userBalance + (+order.priceOrder)
                              //далее проводим зачисление средств на счет исполнителя
                            }).then(createdUser => {res.json(createdUser);});
                          })
                        )
                      })
                    }
                    break;
                    default:
                  }
                })
              })
            }
          );


// модерация заказа ------------------------------------------------------------
          app.put('/createOrder/:_id/moderated', (req, res) =>{
            Orders.findByIdAndUpdate({_id:req.params._id}, req.body)
            .then(function(){
              Orders.findOne({_id:req.params._id}, function(err, order){
                Users.findOne({_id:order.ownerId}, function(err, userOwner){
                  if(userOwner.fToken !== undefined && userOwner.fToken !== null && userOwner.fToken !== ''){
                    sendPush(userOwner.fToken,'orderModeratedNotify');}
                  })
                }).then(createdUser => res.json(createdUser))
              })
            }
          );
// загрузка изображений --------------------------------------------------------
          app.post('/images', (req, res) =>{
            upload(req, res, err => {
              let error =  '';

              if(err){
                if(err.code === 'LIMIT_FILE_SIZE'){
                  error = 'Изображение не более 1мб'
                  res.status(401).json({message: "Размер фото не более 10 мб"});
                }
                if(err.code === 'EXTENTION'){
                  error = 'Формат должен быть jpeg или png'
                  res.status(401).json({message: "Формат должен быть jpeg или png"});
                }
              }
              else {
                const pathphoto = res.req.file.filename
                res.json({
                  pathphoto: pathphoto
                });
              }
            });
          });

          app.get('/', (req, res) => res.send('Hi world'));

          app.listen(3000, () => console.log('Listening on port 3000....'));
          // balance requests routes -----------------------------------------------------
          app.post('/createBalanceRequest', (req, res) =>
          BalanceRequests.create(req.body)
          .then(createBalanceRequest => {
            res.json(createBalanceRequest);
            if(req.body.userId !== 'transaction'){
              sendSms("Новый запрос на пополнение баланса");
            }

          }
        )
      );

// запрос на вывод средств -----------------------------------------------------
      app.post('/createWithdrawalRequest', (req,res) =>
      WithdrawalRequest.create(req.body)
      .then(
        Users.findOne({_id:req.body.userId}, function(err, user){
          Users.findByIdAndUpdate(req.body.userId,
            {userBalance: +user.userBalance - (+req.body.userBalanceWithrawal),
              userBalanceBlock: +user.userBalanceBlock + (+req.body.userBalanceWithrawal)})
              .then(withdrawalRes => {
                res.json(withdrawalRes);
                sendSms("Новый запрос на вывод средств");
              })
            })));
// Получение списка запросов на вывод ------------------------------------------
            app.get('/getWithdrawalRequests' , (req,res) =>
            WithdrawalRequest.find()
            .exec()
            .then(WithdrawalRequest => {
              res.json(WithdrawalRequest);
            })
          );

// Подтверждение заявки на вывод------------------------------------------------
          app.put('/withdrawalRequests/:_id/approved', (req, res) =>{
            WithdrawalRequest.findByIdAndUpdate({_id:req.params._id}, req.body)
            .then(
              WithdrawalRequest.findOne({_id:req.params._id}, function(err, request){

                Users.findOne({_id:request.userId}, function(err, user){
                  console.log(user);
                  Users.findByIdAndUpdate({_id:user._id}, {userBalanceBlock: +user.userBalanceBlock - (+request.userBalanceWithrawal)})
                  .then(createdUser => {
                    res.json(createdUser);
                    if(user.fToken !== undefined && user.fToken !== null && user.fToken !== ''){
                      sendPush(user.fToken,'succesWithdrawal');}
                    })
                  })
                })
              )
            }
          );
// Отклонение заявки на вывод---------------------------------------------------
          app.put('/withdrawalRequests/:_id/reject', (req, res) =>{
            WithdrawalRequest.findByIdAndUpdate({_id:req.params._id}, req.body)
            .then(
              WithdrawalRequest.findOne({_id:req.params._id}, function(err, request){

                Users.findOne({_id:request.userId}, function(err, user){

                  Users.findByIdAndUpdate({_id:user._id},
                    {userBalance: +user.userBalance + (+request.userBalanceWithrawal),
                      userBalanceBlock: +user.userBalanceBlock - (+request.userBalanceWithrawal)

                    }).then(createdUser => {
                      res.json(createdUser);
                      if(user.fToken !== undefined && user.fToken !== null && user.fToken !== ''){
                        sendPush(user.fToken,'rejectWithdrawal');}
                      })

                    })
                  })
                )
              }
            );
// Блокировка средств ----------------------------------------------------------
            app.post('/blokingFunds/:_id',(req,res) =>{
              Users.findOne({_id:req.params._id}, function(err, user){

                Users.findByIdAndUpdate({_id:req.params._id},
                  {userBalance: +user.userBalance - (+req.body.transferPrice),
                    userBalanceBlock: +user.userBalanceBlock + (+req.body.transferPrice)

                  }).then(createdUser => {
                    res.json(createdUser);
                    //  sendPush(user.fToken,'rejectWithdrawal');
                  })
                }
              )
            }
          );
// -----------------------------------------------------------------------------

          app.get('/createBalanceRequest', (req, res) =>
          BalanceRequests.find()
          .exec()
          .then(createBalanceRequest => res.json(createBalanceRequest)),
        );

        app.put('/createBalanceRequest/:_id/canceled', (req, res) =>{
          BalanceRequests.findByIdAndUpdate({_id:req.params._id}, req.body)
          .then(function(){
            BalanceRequests.findOne({_id:req.params._id}).then(
              createdRecord => res.json(createdRecord)
            )
          })
        }
      );
